<?php

namespace Tech\ClearInventoryReservation\Cron;

use Laminas\Log\Logger;
use Laminas\Log\Writer\Stream;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\ScopeInterface;

class ClearInventoryReservation
{
    const SYSTEM_INVENTORY_RESERVATION_ENABLE = 'system/inventory_reservation_cron/enable';

    protected $resourceConnection;

    protected $scopeConfig;

    public function __construct(
        ResourceConnection   $resourceConnection,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {

        $cronEnabled = $this->scopeConfig
            ->getValue(self::SYSTEM_INVENTORY_RESERVATION_ENABLE, ScopeInterface::SCOPE_STORE);
        if ($cronEnabled) {
            $connection = $this->resourceConnection->getConnection();
            $tableName = $this->resourceConnection->getTableName('inventory_reservation');
            $query = $connection
                ->select()
                ->from($tableName);
            $totalItems = $connection->fetchAll($query);
            $connection->truncateTable($tableName);

            $writer = new Stream(BP . '/var/log/cron.log');
            $logger = new Logger();
            $logger->addWriter($writer);
            $logger->info('Totally ' . count($totalItems) . ' Entries was removed from inventory_reservation table');
        }
        return $this;
    }
}
