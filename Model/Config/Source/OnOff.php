<?php

namespace Tech\ClearInventoryReservation\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class OnOff implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('On')],
            ['value' => 0, 'label' => __('Off')]
        ];
    }
}
