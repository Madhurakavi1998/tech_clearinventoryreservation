<?php

namespace Tech\ClearInventoryReservation\Model\Config;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Message\ManagerInterface;

class CronConfig extends Value
{

    const CRON_STRING_PATH = 'inventory_reservation/schedule/cron_expr';

    protected $configValueFactory;

    protected $runModelPath = '';

    public function __construct(
        Context              $context,
        Registry             $registry,
        ScopeConfigInterface $config,
        TypeListInterface    $cacheTypeList,
        AbstractResource     $resource = null,
        AbstractDb           $resourceCollection = null,
        ValueFactory         $configValueFactory,
        ManagerInterface $messageManager,
        $runModelPath = '',
        array                $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->configValueFactory = $configValueFactory;
        $this->runModelPath = $runModelPath;
        $this->messageManager = $messageManager;
    }

    public function afterSave()
    {
        $minute = $this->getValue();
        $cronStr = sprintf('*/%s * * * *', $minute);
        if ($minute == 0) {
            $cronStr = '';
        }

        try {
            $this->configValueFactory->create()->load(
                self::CRON_STRING_PATH,
                'path'
            )->setValue(
                $cronStr
            )->setPath(
                self::CRON_STRING_PATH
            )->save();

        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage(__('We can\'t save the cron expression.'));
        }

        return parent::afterSave();
    }
}
