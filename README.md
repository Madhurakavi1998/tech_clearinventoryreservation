# Tech_ClearInventoryReservation

The configuration path is Stores -> Configuration -> Advanced -> System -> Cron configuration options for group:clear_inventory_reservation

Two basic configurations were created.
 * Enable Cron Job
 * Generate Schedules Every(minute)

 Please enable " Enable Cron Job" and set cron time in " Generate Schedules Every(minute)"

    bin/magento cron:install (Run, if Crontab hasn't been generated )
 
    bin/magento cron:run --group default  (You must run cron twice: the first time to discover tasks to run and the second time — to run the tasks themselves. The second cron run must occur on or after the scheduled_at time for every task.)
 
 We can add more cron configuration and its functionality like core. But It will take more time to develop

The cron message display on magento/var/log/cron.log

Cron message: "2021-10-11T13:38:12+00:00 INFO (6): Totally 2 Entries was removed from inventory_reservation table"

 Magento code standard(PHPCS and PHPMD) followed and tested with magento2.4.x and magento2.3.x
